angular.module('ngCourse')

.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('dashboard', {
            url: '/',
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardCtrl',
            resolve: {
                lists: function (ListResource) {
                    return ListResource.query().$promise;
                }
            }
        })
        .state('dashboard.list', {
            url: ':id',
            templateUrl: 'views/list.html',
            controller: 'ListCtrl',
            resolve: {
                list: function (ListResource, $stateParams) {
                    
                    console.log();
                    if (!$stateParams.id)
                        return new ListResource({
                            name: 'New TrackList',
                            image: '',
                            category: 'undefined',
                            songs: []
                        });
                    return ListResource.get({}, {
                        id: $stateParams.id
                    }).$promise;
                }
            }
        });
});
