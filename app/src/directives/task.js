angular.module('ngCourse')

.directive('songs', function (ListResource, $stateParams) {
    return {
        restrict: 'E',
        templateUrl: 'views/list.html',
        scope: {
            list: '=model'
        },
        controller: function ($scope) {
            $scope.toggle = function (task) {
                var resource = new ListResource($scope.task);
                resource.$update();
            };

        }
    };
});
