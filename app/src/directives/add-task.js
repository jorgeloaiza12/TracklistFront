angular.module('ngCourse')

.directive('addTask', function (ListResource, $stateParams) {
    return {
        restrict: 'E',
        templateUrl: 'views/add-task.html',
        replace: true,
        scope: { songs: '=model' },
        link: function (scope, element, attrs, ctrl) {
            element.find('input').on('keydown', function (e) {
                if (e.keyCode == 13 && scope.title) {
                    scope.$apply(function () {
                        var task = new ListResource({
                            text: scope.title,
                            done: false
                        });
                        scope.songs.push(task);

                        task.$save({
                            list: scope.$parent.list.id
                        });
                        scope.title = '';
                    });
                }
            });

            //scope.checkAll = function () {
            //    scope.songs.forEach(function (task) {
            //        if (!task.done) {
            //            task.done = true;
            //            scope.$emit('task-toggle', true);
            //        }
            //   });
            //};
        }
    };
});
