angular.module('ngCourse')

.factory('ListResource', function ($resource) {
    var res = $resource('http://localhost:3000/playlist/:id', {
        id: '@id'
    });

    return res;
});
