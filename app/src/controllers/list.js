angular.module('ngCourse')

.controller('ListCtrl', function ($scope, $state, list) {
    $scope.list = list;

    $scope.saveList = function () {
        $scope.list.$save();
    };

    $scope.addSong = function (title, author, genre, cover, mp3) {
        $scope.list.songs.push({
            title: title,
            author: author,
            genre: genre,
            cover: cover,
            mp3: mp3
        });
    };

    //$scope.completedCount = $scope.list.songs.filter(function (task) {
    //    return task.done;
    //}).length;

    //$scope.$on('task-toggle', function (e, status) {
    //    var delta = !!status ? 1 : -1;
    //    $scope.completedCount += delta;
    //});
});
